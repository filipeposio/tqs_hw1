package pt.ua.tqs.tqskeychain;


import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.SecretKeySpec;


/**
 * A text-oriented keychain to keep a list of passwords in a file
 *
 * @author tqs student
 */
public class TqsKeychain {
    private static final String KEYCHAIN = "Keychain.txt";
    static final Scanner s = new Scanner(System.in);
    static final  File keys = new File(KEYCHAIN);
    private TqsKeychain(){}
    
    public static void main(String[] args) throws Exception {
       

        Scanner scFile;

        int op;
        do {
            Logger logger = Logger.getAnonymousLogger();
            logger.log(Level.SEVERE,"1- Criar nova entrada");
            logger.log(Level.SEVERE,"2- Listar keychain");
            logger.log(Level.SEVERE,"3- Pesquisar credenciais p/ aplicação");
            logger.log(Level.SEVERE,"4- Cifrar Ficheiro");
            logger.log(Level.SEVERE,"5- Decifrar Ficheiro");
            logger.log(Level.SEVERE,"0- Sair ");
            logger.log(Level.SEVERE,"Opção? ");
            op = s.nextInt();
            logger.log(Level.SEVERE,"\n");
           
            switch (op) {
                case 1:
                    logger.log(Level.SEVERE,"OPCAO 1");
                    StringBuilder bld = new StringBuilder();
                    logger.log(Level.SEVERE,"Nome da aplicação/site? ");
                    String platform = s.next();
                    bld.append(platform);
                    bld.append(",");
                    logger.log(Level.SEVERE,"Utilizador? ");
                    String user = s.next();
                    bld.append(user);
                    bld.append(",");
                    logger.log(Level.SEVERE,"Gerar password? (y/n)");
                    String yn = s.next();
                    if ("y".equals(yn) || "Y".equals(yn)) {
                        String generatedPass = nextSessionPass();
                        logger.log(Level.SEVERE,"Nova Pass > " + generatedPass);
                        bld.append(generatedPass);
                    } else if ("n".equals(yn) || "N".equals(yn)) {
                        logger.log(Level.SEVERE,"Digite Pass");
                        String pass = s.next();
                        bld.append(pass);
                    } else {
                       logger.log(Level.SEVERE,"opcao incorrecta");
                        break;
                    }

                    FileWriter fw = new FileWriter(keys, true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(bld.toString());
                    bw.newLine();
                    bw.close();

                    logger.log(Level.SEVERE,"Utilizador guardado.");
                    logger.log(Level.SEVERE,"\n");
                    break;

                case 2:

                    scFile = new Scanner(keys);
                    logger.log(Level.SEVERE,"Aplicação       User          Password");
                    logger.log(Level.SEVERE,"-------------------------------------");
                    while (scFile.hasNextLine()) {
                        String entry = scFile.nextLine();
                        String[] words = entry.trim().split(",");
                        System.out.printf("%-10s  %8s  %15s", words[0], words[1], words[2]);
                        logger.log(Level.SEVERE,"\n");

                    }
                    scFile.close();
                    break;

                case 3:
                    logger.log(Level.SEVERE,"OPCAO 3");
                    logger.log(Level.SEVERE,"Aplicação a procurar? ");
                    String search = s.next();

                    scFile = new Scanner(keys);
                    String[] palavras;
                    logger.log(Level.SEVERE,"Platform       User          Password");
                    logger.log(Level.SEVERE,"-------------------------------------");

                    while (scFile.hasNextLine()) {
                        palavras = scFile.nextLine().split(",");
                        if (palavras[0].startsWith(search)) {
                            System.out.printf("%-10s  %8s  %15s", palavras[0], palavras[1], palavras[2]);
                            logger.log(Level.SEVERE,"\n");
                        }

                    }

                    scFile.close();

                    logger.log(Level.SEVERE,"\n");
                    break;

                case 4:
                    logger.log(Level.SEVERE,"OPCAO 4");
                    FileInputStream fis= null;
                    FileOutputStream fos = null;
                    CipherInputStream cis = null; 
                    try {
                       
                        String key = "MySEcRetKeY";
                        StringBuilder bldr = new StringBuilder();
                        bldr.append(key);
                        int length = key.length();
                        if (length > 16 && length != 16) {
                            key = key.substring(0, 15);
                        }
                        bldr.append(key);
                        if (length < 16 && length != 16) {
                            for (int i = 0; i < 16 - length; i++) {
                                bldr.append("0");
                            }
                        }
                        key = bldr.toString();
                        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
                        Cipher encrypt = Cipher.getInstance("AES/ECB/PKCS5Padding", "SunJCE");
                        encrypt.init(Cipher.ENCRYPT_MODE, secretKey);
                        try {
                            fis = new FileInputStream(KEYCHAIN);
                            cis = new CipherInputStream(fis, encrypt);
                            fos = new FileOutputStream("Keychain-cifrado.txt");
                            byte[] b = new byte[8];
                            int i = cis.read(b);
                            while (i != -1) {
                                fos.write(b, 0, i);
                                i = cis.read(b);
                            }
                        } catch (IOException err) {
                            throw new IOException(err);
                        }
                        
                    } catch (Exception e) {
                        
                        logger.log(Level.SEVERE, "an exception was thrown", e);
                    }finally{
                            if(fis!=null)
                                fis.close();
                            if(fos!=null)
                                fos.close();
                            if(cis!=null)
                                cis.close();
                        }
                    
                    logger.log(Level.SEVERE,"Ficheiro Cifrado em Keychain-cifrado.txt");
                    logger.log(Level.SEVERE,"\n");
                    break;

                case 5:
                    logger.log(Level.SEVERE,"OPCAO 5");
                    FileInputStream fis1 = null;
                    FileOutputStream fos1= null;
                    CipherInputStream cis1= null;
                    try {
                        File aesFile = new File("Keychain-cifrado.txt");
                        File aesFileBis = new File(KEYCHAIN);
                        
                        String key = "MySEcRetKeY";
                        StringBuilder bldr = new StringBuilder();
                        int length = key.length();
                        if (length > 16 && length != 16) {
                            key = key.substring(0, 15);
                        }
                        bldr.append(key);
                        if (length < 16 && length != 16) {
                            for (int i = 0; i < 16 - length; i++) {
                                bldr.append("0");
                            }
                        }
                        key = bldr.toString();

                        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
                        Cipher decrypt = Cipher.getInstance("AES/ECB/PKCS5Padding", "SunJCE");

                        decrypt.init(Cipher.DECRYPT_MODE, secretKey);
                        fis1 = new FileInputStream(aesFile);
                        cis1 = new CipherInputStream(fis1, decrypt); 
                        fos1 = new FileOutputStream(aesFileBis);
                        byte[] b = new byte[8];
                        int i = cis1.read(b);
                        while (i != -1) {
                            fos1.write(b, 0, i);
                            i = cis1.read(b);
                        }
                    } catch (Exception e) {
                        
                        logger.log(Level.SEVERE, "an exception was thrown", e);
                    }finally{
                        if(fis1!=null)
                            fis1.close();
                        if(cis1!=null)
                            cis1.close();
                        if(fos1!=null)
                            fos1.close();
                    }
                    logger.log(Level.SEVERE,"Decifrado");
                    logger.log(Level.SEVERE,"\n");
                    break;

                case 0:
                    break;
                default:
                    logger.log(Level.SEVERE,"Nao existe tal opcao");
                    featureOne();
            }
        } while (op != 0);

    }
    private static void featureOne(){
        int x =0;
        x = x + 1 ;
        Logger logger = Logger.getAnonymousLogger();
        logger.log(Level.FINE,"X="+x);
       
    }
    // Criar uma password complexa Alphanumerica
    public static String nextSessionPass() {
        Random rand = new Random();
        return new BigInteger(90, rand).toString(32);
    }
}
